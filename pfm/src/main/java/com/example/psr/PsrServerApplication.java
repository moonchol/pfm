package com.example.psr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PsrServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PsrServerApplication.class, args);
    }
}
