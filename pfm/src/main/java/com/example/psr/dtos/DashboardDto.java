package com.example.psr.dtos;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
public class DashboardDto {
	
	private String incomeTotal;
	
	private String expendsTotal;
	
	private String assetsTotal;
	
	private String liabilityTotal;
	
	private List<assetDto> assets;
	
	private List<expendDto> expend;
	
	private List<incomeDto> income;
	
	private List<liabilityDto> liabilities;
	

}
