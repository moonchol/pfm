package com.example.psr.dtos;

import lombok.Data;

@Data
public class liabilityDto {
	private String item;
	private String value;
	private String regist_date;
}
