package com.example.psr.entity.Cards;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cards")
public class cardsEntity {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "type")
    private String type;
    
    @Column(name="regist_time")
    private Date regist_time;
    
    @Column(name="update_time")
    private Date update_time;
    
    @Column(name="delte_time")
    private Date delete_time;

}
