package com.example.psr.entity.IncomeHistory;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "income_history")
public class incomeHistoryEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "price")
	private Double price;

	@Column(name = "user")
	private String userName;

	@Column(name = "regist_time")
	private Date regist_time;

	@Column(name = "update_time")
	private Date update_time;

	@Column(name = "delete_time")
	private Date delete_time;

}
