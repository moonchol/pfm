package com.example.psr.entity.Liabilities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.psr.entity.Income.IncomeEntity;

import lombok.Data;

@Data
@Entity
@Table(name = "Liability")
public class LiabilitiesEntity {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "item")
    private String item;
    
    @Column(name="regist_time")
    private Date regist_time;
    
    @Column(name="update_time")
    private Date update_time;
    
    @Column(name="delete_time")
    private Date delete_time;

}
