package com.example.psr.entity.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "userdao")
public class UserIdEntity {

    @Id
    @Column(name = "id")
    private Integer ID;

    @Column(nullable = false, name = "Username")
    private String username;

    @Column(nullable = false, name = "Password")
    private String password;

    @Column(name = "Kengen")
    private Integer Kengen;

}
