package com.example.psr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.psr.entity.Expends.expendEntity;

@Repository
public interface ExpendsRepository extends JpaRepository<expendEntity, Long> {

}
