package com.example.psr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.psr.entity.IncomeHistory.incomeHistoryEntity;

@Repository
public interface IncomeHistoryRepository extends JpaRepository<incomeHistoryEntity, Long> {

}
