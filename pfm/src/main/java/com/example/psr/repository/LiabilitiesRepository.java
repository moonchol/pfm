package com.example.psr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.psr.entity.Liabilities.LiabilitiesEntity;

@Repository
public interface LiabilitiesRepository extends JpaRepository<LiabilitiesEntity, Long> {

}
