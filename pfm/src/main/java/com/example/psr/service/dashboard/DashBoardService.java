package com.example.psr.service.dashboard;

import org.springframework.stereotype.Component;

import com.example.psr.dtos.DashboardDto;

public interface DashBoardService {
	
	/**
	 * Dashboardに表示するデーターを取得する.
	 * 
	 * @return dashboard
	 */
	public DashboardDto getDashboardData();
	
	
	

}
