package com.example.psr.service.dashboard.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.psr.dtos.DashboardDto;
import com.example.psr.entity.ExpendsHistory.expendsHistoryEntity;
import com.example.psr.entity.IncomeHistory.incomeHistoryEntity;
import com.example.psr.repository.ExpendsHistoryRepository;
import com.example.psr.repository.IncomeHistoryRepository;
import com.example.psr.service.dashboard.DashBoardService;

@Service("dashboardService")
public class DashBoardServiceImpl implements DashBoardService {

	@Autowired
	private IncomeHistoryRepository incomeHistoryRepository;

	@Autowired
	private ExpendsHistoryRepository expendsHistoryRepository;

	@Override
	public DashboardDto getDashboardData() {
		// get income data
		List<incomeHistoryEntity> incomes = incomeHistoryRepository.findAll();
		// get expend data
		List<expendsHistoryEntity> expends = expendsHistoryRepository.findAll();
		
		DashboardDto dashboardDto = new DashboardDto();

		double income_total = 0.0;

		if (incomes.isEmpty()) {
			income_total = incomes.stream()
					.map(p -> p.getPrice())
					.mapToDouble(v -> v.doubleValue())
					.sum();
		}
		
		dashboardDto.setIncomeTotal(String.valueOf(income_total));
		
		return dashboardDto;
	}

}
