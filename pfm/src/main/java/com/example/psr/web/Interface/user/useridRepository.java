package com.example.psr.web.Interface.user;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.psr.entity.User.UserIdEntity;

public interface useridRepository extends JpaRepository<UserIdEntity, Integer>{
	UserIdEntity findByUsername(String username);
}
