package com.example.psr.web.controller.dashBoard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.psr.dtos.DashboardDto;
import com.example.psr.service.dashboard.DashBoardService;
import com.example.psr.web.controller.base.baseController;

@RestController
@RequestMapping(value = "/dashboard")
public class dashBoardController implements baseController {
	
	@Autowired
	private DashBoardService dashBoardService; 
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<DashboardDto> getDashBoardData() {
		final DashboardDto dashboardData = dashBoardService.getDashboardData();
		
		if(dashboardData == null) {
			return new ResponseEntity<DashboardDto>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<DashboardDto>(dashboardData, HttpStatus.OK);
	}

}
