 package com.example.psr.web.controller.skipPage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class welcomePFS {

	/**
	 * personFMS
	 *
	 * @return skipPage
	 */
	@GetMapping("/PersonFMS")
	public String skipMain() {
		
		return "skipPage";
	}
}
