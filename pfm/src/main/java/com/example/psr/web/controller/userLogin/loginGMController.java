package com.example.demo.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.example.demo.Interface.UseridRepository;
import com.example.demo.entity.UserIdEntity;


@Controller
public class loginGMController {
	
	@Autowired
	private UseridRepository userRepository;

	@GetMapping("/userlogin")
	public String UserLogin(){
		
		System.out.println("this is test");
		return "welcome";
		
	}
	
	/*
	 * ユーザーログインチェック
	 */
	@PostMapping("/loginform")
	public String loginckeck(String inputEmail, String inputPassword, HttpSession session) {
		
		UserIdEntity userentity = userRepository.findByUsername(inputEmail);
		
		if (userentity == null ) {
			return "welcome";
		}
		
		if (!inputPassword.equals(userentity.getPassWord())) {
			return "redirect:/userlogin";
		}
		
		System.out.println(userentity.getKengen());
		session.setAttribute("user", userentity);
		return "masteredit";
	}
	
}
